public class School extends EducationInstitute {

	public School(String name, String address, int id) {
		super(name, address, id);
		this.TAX = 50;

	}

	public String toString() {
		return "School [TAX=" + TAX + "]";
	}

}