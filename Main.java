import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		List<EducationInstitute> allInstitutes = new ArrayList<>();
		
		Student a = new Student("Maria", 16, "female");
		Student b = new Student("Boris", 12, "male");
		Student c = new Student("Ivan", 21, "male");
		Student a1 = new Student("Kameliya", 16, "female");
		Student b1 = new Student("Dimitar", 12, "male");
		Student c1 = new Student("Hristiyan", 15, "male");
		Student a2 = new Student("Nikol", 16, "female");
		Student b2 = new Student("Panko", 12, "male");
		Student c2 = new Student("Darena", 21, "female");
		Student a3 = new Student("Donika", 20, "female");
		Student b3 = new Student("Rumen", 14, "male");
		Student c3 = new Student("Neli", 20, "female");
		
		EducationInstitute z = new School("Hr. Botev", "Chayka 25", 1);
		EducationInstitute w = new School("Neofit Bozvoli", "Liubcho Patronov 3", 1);
		EducationInstitute p = new School("SU Geo Milev", "bul. Republica", 1);
		EducationInstitute r = new University(" TY-VARNA ", " Studentska ", 1);
		EducationInstitute y = new University("Varna Free University", "Alen Mak", 2);
		
		allInstitutes.add(z);
		allInstitutes.add(y);
		allInstitutes.add(w);
		allInstitutes.add(p);
		allInstitutes.add(r);
		
		z.enrollStudent(a);
		y.enrollStudent(b);
		y.enrollStudent(c);
		w.enrollStudent(a1);
		w.enrollStudent(b1);
		z.enrollStudent(c1);
		p.enrollStudent(a2);
		p.enrollStudent(b2);
		r.enrollStudent(c2);
		r.enrollStudent(a3);
		
		
		System.out.println("Total income of " + z.getName() + " is " + z.getTotalIncome());
		y.getAverageGrade();
		

		Student topStudent = new Student();
		for (EducationInstitute e : allInstitutes) {
			if (e.getTopPerformer().getGrade() > topStudent.getGrade())
				topStudent = e.getTopPerformer();
		}
		System.out.println("The top performer among ALL Education Institutes is: " + topStudent.getNameStudent()
				+ " with grade of " + topStudent.getGrade());

		

	}

}