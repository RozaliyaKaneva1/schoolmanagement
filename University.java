public class University extends EducationInstitute {

	public University(String name, String address, int id) {
		super(name, address, id);
		this.TAX = 100;

	}

	public String toString() {
		return "University [TAX=" + TAX;
	}

}