import java.util.Random;

public class Student {

	protected String nameStudent;
	EducationInstitute entityId;
	protected int age;
	private String gender;
	double grade;

	public Student() {
	}

	public Student(String nameStudent, int age, String gender) {

		this.nameStudent = nameStudent;
		this.age = age;
		this.gender = gender;
		this.grade = Math.round(gradeMethod() * 100d) / 100d;
		;

	}

	public String getNameStudent() {
		return nameStudent;
	}

	public void setNameStudent(String nameStudent) {
		this.nameStudent = nameStudent;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}

	public double gradeMethod() {
		double min = 2;
		double max = 6;
		double random = new Random().nextDouble();
		double result = min + (random * (max - min));

		return result;

	}

	public EducationInstitute getEntityID() {
		return entityId;
	}

	public void setEntityID(EducationInstitute entityID) {
		entityId = entityID;
	}

	public double calculatePayment() {
		return ((age / entityId.getAverageGrade()) * 100 + entityId.getTAX());
	}
}