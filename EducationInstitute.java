import java.util.ArrayList;
import java.util.List;

public class EducationInstitute {

	public String name;
	private String address;
	int id;
	List<Student> students;
	protected int TAX;

	public EducationInstitute(String name, String address, int id) {
		this.name = name;
		this.address = address;
		this.id = id;
		students = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String toString() {
		return "EducationInstitute: name=" + name + ", address=" + address + ", id=" + id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTAX() {
		return TAX;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public void enrollStudent(Student s) {
		students.add(s);
		System.out.println("Hello, " + s.getNameStudent() + ", and welcome to " + getName());
		s.setEntityID(this);
	}

	public double getAverageGrade() {
		double result = 0;
		for (Student st : students) {
			result += st.getGrade();
		}    
		result /= students.size();
		System.out.println("The average grade of the students in " + getName() + " is " + Math.round(result * 100d) / 100d);
		return result;
	}

	public Student getTopPerformer() {
		Student male = getTopMale();
		Student female = getTopFemale();
		if (male.getGrade() > female.getGrade()) {
			return male;
		}
		return female;
	}

	public Student getTopMale() {
		double bestScore = 0;
		Student topStudent = new Student();
		for (Student s : students) {
			if (s.getGender().equals("male")) {
				if (s.getGrade() > bestScore) {
					bestScore = s.getGrade();
					topStudent = s;
				}
			}
		}
		return topStudent;
	}

	public Student getTopFemale() {
		double bestScore = 0;
		Student topStudent = new Student();
		for (Student s : students) {
			if (s.getGender().equals("female")) {
				if (s.getGrade() > bestScore) {
					bestScore = s.getGrade();
					topStudent = s;
				}
			}
		}
		return topStudent;
	}

	public Student getTopContributor() {
		double topPayment = 0;
		Student topStudent = new Student();
		for (Student s : students) {
			if (s.calculatePayment() > topPayment) {
				topPayment = s.calculatePayment();
				topStudent = s;
			}
		}
		return topStudent;
	}

	public double getTotalIncome() {
		double total = 0;
		for (Student s : students) {
			total +=  s.calculatePayment();
		}
		return Math.round(total * 100d) / 100d;
	}

}